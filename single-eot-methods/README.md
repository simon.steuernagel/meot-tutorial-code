# Single Extended Object Tracking

This directory contains examples for extended object tracking in the case that measurements are only observed from a single object.

For each algorithm, a similar basic notebook exists. Each of these contains the same data generation format.

Three algorithms are implemented, across four notebooks:

- [RM](./single-eot-methods/ch3.1.1_RM stationary.ipynb): Very simple example implementing the Random Matrix algorithm for tracking a stationary object with measurements from the entire object surface.
- [MEM-EKF*](./single-eot-methods/ch3.2.1_MEM-EKF stationary.ipynb): Very simple example implementing the MEM-EKF* algorithm for tracking a stationary object with measurements from the entire object surface.
- [IAE](./single-eot-methods/ch3.2.2_IAE.ipynb): Simple example implementing the Independent Axis Estimation Algorithm for tracking an object moving in a straight line with measurements from the entire object surface.
- [Stationary Simple-GP-RHM](./single-eot-methods/ch4.3.1_Simple-GP-RHM stationary.ipynb): Very simple example for Gaussian Process based Random Hypersurface models, consisting of tracking a single stationary rectangular object with measurements from the object contour.
- [Moving Simple-GP-RHM](./single-eot-methods/ch4.3.2_Simple-GP-RHM moving.ipynb): Slightly more complex example for Gaussian Process based Random Hypersurface models, consisting of tracking a cross-shaped object moving along a known axis with measurements from the object surface.

Additionally, one algorithm from a recent work that is not included in the tutorial is demonstrated:
- [VBRM](./single-eot-methods/ch3Bonus_VBRM.ipynb): Variant of the Random Matrix model that makes use of the variational bayes technique. The example used for demonstration consists of a rectangular moving object generating measurements from the entire object surface. 