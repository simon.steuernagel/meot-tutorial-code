# Supplementary Code for A Tutorial On Multiple Extended Object Tracking
 
Example Python Notebooks for "A Tutorial On Multiple Extended Object Tracking" by Granström and Baum.

## Table of Contents
The structure of this repository closely follows the paper. Individual notebooks focus on a single example for an algorithm, in order to keep them short and concise. Therefore, code for e.g. algorithms will be included in multiple notebooks.

#### Single Extended Object Tracking

Companion notebooks for chapters 2 to 5 are in the subdirectory [single-eot-methods](./single-eot-methods/). 
The content in these directly corresponds to one sub-chapter of the paper. The following notebooks are included:
- [RM](./single-eot-methods/ch3.1.1_RM stationary.ipynb): Very simple example implementing the Random Matrix algorithm for tracking a stationary object with measurements from the entire object surface.
- [MEM-EKF*](./single-eot-methods/ch3.2.1_MEM-EKF stationary.ipynb): Very simple example implementing the MEM-EKF* algorithm for tracking a stationary object with measurements from the entire object surface.
- [IAE](./single-eot-methods/ch3.2.2_IAE.ipynb): Simple example implementing the Independent Axis Estimation Algorithm for tracking an object moving in a straight line with measurements from the entire object surface.
- [Stationary Simple-GP-RHM](./single-eot-methods/ch4.3.1_Simple-GP-RHM stationary.ipynb): Very simple example for Gaussian Process based Random Hypersurface models, consisting of tracking a single stationary rectangular object with measurements from the object contour.
- [Moving Simple-GP-RHM](./single-eot-methods/ch4.3.2_Simple-GP-RHM moving.ipynb): Slightly more complex example for Gaussian Process based Random Hypersurface models, consisting of tracking a cross-shaped object moving along a known axis with measurements from the object surface.

Additionally, one algorithm from a recent work that is not included in the tutorial is demonstrated:
- [VBRM](./single-eot-methods/ch3Bonus_VBRM.ipynb): Variant of the Random Matrix model that makes use of the variational bayes technique. The example used for demonstration consists of a rectangular moving object generating measurements from the entire object surface. 

#### Metrics for Comparing (Single) Extended object trackers
Companion notebooks for chapter 6 can be found in the subdirectory [eot-metrics](./eot-metrics/). 
-  [Gauss-Wasserstein for Ellipses](./eot-metrics/ch6.1_GW ellipses.ipynb): Comparison of the Gauss-Wasserstein and the Euclidean distance for Elliptical Extended Objects.

- [Interactive Gauss-Wasserstein for Ellipses](./eot-metrics/ch6.1b_interactive GW.ipynb): Interactive Widget allowing comparison of Gauss-Wasserstein and Euclidean distances between ellipses. Please note that this can **not** be used in browser.

- [Wasserstein for Star-Convex Objects](./eot-metrics/ch6.2_Star-Convex Objects.ipynb): Application of the (discrete) Wasserstein distance for evaluation of tracking results of a Star-Convex object.

#### Data Association for Multiple Extended Object Tracking

Companion methods for chapter 7 through 10 can be found in the subdirectory [association-methods](./association-methods/).

---

```
TODO insert paper citation here 
```
