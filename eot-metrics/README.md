# Metrics for comparing (Single) Extended Object trackers

This directory contains example notebooks for chapter 6: Metrics for comparing (Single) Extended Object trackers.

Three notebooks are included, two which deal with elliptical targets and one that deals with the case of star-convex targets:

[ch6.1_GW ellipses](./ch6.1_GW ellipses.ipynb) implements the Gauss-Wasserstein (GW) distance for elliptical extended objects and compares it to the Euclidean distance applied directly to the parameters. Two pre-defined examples are shown: First, three elliptical objects are compared by means of both distance functions. 
Furthermore, the notebook includes animations that show the change in distance as a single parameter (such as orientation) gets changed.

[ch6.1b_interactive GW](./ch6.1b_interactive GW.ipynb) consists of an interactive widget that allows parameterization of two ellipses, while simultaneously showing the change in GW and Euclidean distances causes by recent changes in parameters. Note that this notebook can not be viewed in your Browser, you will need to manually run it for the widget to work.

[ch6.2_Star-Convex Objects](./ch6.2_Star-Convex Objects.ipynb) applies the (discrete) Wasserstein distance to evaluate the results produces by the Simple-GP-RHM algorithm when applied to a cross-shaped, moving object. Data generation and algorithm application is highly similar to [the corresponding GP notebook](../single-eot-methods/ch4.3.2_Simple-GP-RHM moving.ipynb).
